const autoprefixer = require('autoprefixer');
const inlineSVG = require('postcss-inline-svg');
const replace = require('postcss-selector-replace');

module.exports = (ctx) => ({
  context: 'dist',
  map: process.env.NODE_ENV === 'production' ? false : 'inline',
  plugins: ctx.file.basename === 'ie11.css' ? [
    autoprefixer({
      grid: 'autoplace'
    })
  ] : [
    inlineSVG({
      paths: ['src/', 'node_modules/bootstrap-icons/icons']
    }),
    replace({
      before: [".ck-content body", ".ck-content html", ".ck-content .cke_editable", ".ck-content .cke_editable"],
      after: [".ck-content", ".ck-content", ".ck-content", ".ck-content"]
    })
  ]
});
